﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Data.Common;

namespace DemoBdd
{
    class Program
    {
        static void Main(string[] args)
        {
            Demo1();

            Console.WriteLine(GetTailleMoyenne());

            Console.ReadLine();
        }

        private static float GetTailleMoyenne()
        {
            // 1 - Configurer une connection
            IDbConnection cnx = new MySqlConnection();
            cnx.ConnectionString = "Server=localhost;Database=goelands;Uid=root;Pwd=root;";

            // 2 - Préparer une commande SQL
            IDbCommand cmd = new MySqlCommand();
            cmd.CommandText = @"SELECT AVG(taille) 
                                FROM adherent";
            cmd.Connection = cnx;
            float resultat = 0;

            try
            {
                // 3 - Ouvrir la connection
                cnx.Open();

                // 4 - Exécuter la commande
                resultat = Convert.ToSingle(cmd.ExecuteScalar());
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // 6 - Fermer la connection
                cnx.Close();
            }

            return resultat;
        }

        private static float GetAgeMoyen()
        {
            return 0;
        }

        private static void Demo1()
        {
            // 1 - Configurer une connection
            IDbConnection cnx = new MySqlConnection();
            cnx.ConnectionString = "Server=localhost;Database=goelands;Uid=root;Pwd=root;";

            // 2 - Préparer une commande SQL
            IDbCommand cmd = new MySqlCommand();
            cmd.CommandText = "SELECT * FROM adherent";
            cmd.Connection = cnx;

            try
            {
                // 3 - Ouvrir la connection
                cnx.Open();

                // 4 - Exécuter la commande
                IDataReader dr = cmd.ExecuteReader();

                // 5 - Traiter le résultat
                while (dr.Read())
                {
                    // afficher le nom :
                    int id = dr.GetInt32(dr.GetOrdinal("id"));
                    string nom = dr.GetString(dr.GetOrdinal("nom"));
                    DateTime dateNaissance = dr.GetDateTime(dr.GetOrdinal("date_naissance"));
                    Console.WriteLine(id + " : " + nom + " né(e) le " + dateNaissance);
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                // 6 - Fermer la connection
                cnx.Close();
            }
        }
    }
}
