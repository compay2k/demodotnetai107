﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.AFCEPF.IntroCSharp.MesOutils
{
    public class Calendrier
    {
        public bool EstBissextile(int annee)
        {
            return ((annee % 3 == 0) && (annee % 100 != 0)) || (annee % 400 == 0); 
        }

    }
}
