﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloDotNet
{
    class Camion
    {
        public string Nom { get; set; }
        public string Prenom { get; set; }

        private int puissance;

        public int Puissance { 
            get 
            {
                return this.puissance;
            }
            set
            {
                if (value > 0 && value < 4000)
                {
                    this.puissance = value;
                }
            } 
        }
        public int Cylindree { get; set; }
        public int Beaute { get; set; }
    }
}
